## Problem
[Brace expansion](http://linuxcommand.org/lc3_lts0080.php) is a neat (cool) thing available in the terminal. Try `echo {a,b,c}-{d,e}`. It outputs combinations just like in [backtracking2](/backtracking/backtracking2) and even supports ranges, letting you do things like `mkdir {2009..2012}-{10..12}-{1..6}` easily. It can also be recursive, as in `echo {a,b{1{z,c},2,3}B,c}-{f,g{6,7}}`. The task then is to implement that functionality, given such a string. Let's assume that there are no ranges (no `..`), no spaces, and that the expression is valid (each `'{'` has a matching `'}'`), delimited only by commas (`','`).

Sample input ([backtracking4.in](bracktracking4.in)):
```
{a,b{1{z,c},2,3}B,c}-{f,g{6,7}}
{a-b-c}-{d,e}
abc{d,e,f}-h{gh,ij{k,l}z,n}-op{1,2{3,4,{56,78}}}
g{a,b,c}hi
{a,b,c}
{a,b}-{d,e,f}
{g,h,i}-{d,e,fg{1,2,3}}
```

[backtracking4.py](backtracking4.py) has been provided to generate accepted output for your input file. [Redirect](http://linuxcommand.org/lts0060.php) input and output like so:
```
python backtracking4.py < backtracking4.in > diff2.txt
```

Similarly, redirect the output of your code,
```
./a.out < backtracking4.in > diff1.txt
```

and then finally `diff` the outputs:
```
diff diff1.txt diff2.txt
```
If your code is correct, then the two files should be the same and `diff` would have no output. Alternatively, you could compare the outputs by using a site like [text-compare.com](http://text-compare.com).
