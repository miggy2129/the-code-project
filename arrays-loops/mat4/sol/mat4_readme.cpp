// mat4_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int arr[MAX_N][MAX_N];
    int N;

    char name[] = "mat4";

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                scanf("%d", &arr[i][j]);
            }
        }
        printf("Input:\n```\n%d\n", N);
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                printf("%d ", arr[i][j]);
            }
            printf("\n");
        }
        printf("```\nOutput:\n```\n");
        // Your code here
        for(int i = 0; i<N; i++){
            for(int j = i+1; j<N; j++){
                swap(arr[i][j], arr[j][i]);
            }
        }

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                printf("%d ", arr[i][j]);
            }
            printf("\n");
        }
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output)\n", name,name,name,name,name,name );

}