void f(n=5){
    if(n == 0) { 
        alert(n);
        return;
    }

    alert(n);
    f(n-1);
    alert(n);
}