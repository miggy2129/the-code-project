## Loop 2. Print an NxM grid.

Print `N` rows of `M` characters of `'*'`.

Input:
```
3 2
```
Output:
```
**
**
**
```
Input:
```
5 1
```
Output:
```
*
*
*
*
*
```
Input:
```
4 4
```
Output:
```
****
****
****
****
```

See [loop2.cpp](loop2.cpp) and [loop2.in](loop2.in). Expected output is in [loop2.output](loop2.output).