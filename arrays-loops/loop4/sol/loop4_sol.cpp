// loop4_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int N;

    while(scanf("%d", &N)!=EOF){
        // Your code here
        for(int i = 0; i<N; i++){
            for(int j = 0; j<i; j++){
                printf(" ");
            }
            for(int j = 0; j<(N-i); j++){
                printf("*");
            }
            printf("\n");
        }
        // Or
        // for(int i = 1; i<=N; i++){
        //     int numStar = N - i + 1;
        //     int numSpace = N - numStar;
        //     for(int j = 1; j<=numSpace; j++){
        //         printf(" ");
        //     }
        //     for(int j = 1; j<=numStar; j++){
        //         printf("*");
        //     }
        //     printf("\n");
        // }

    }

    return 0;
}