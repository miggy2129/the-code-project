// deque.cpp
#include <iostream>
#include <deque>

using namespace std;

int main(){
    deque<int> dq;  // []; empty
    dq.push_back(1); // [1]
    dq.push_back(7);  // [1,7]
    dq.push_back(4);  // [1,7,4]
    for(int i = 0; i<dq.size(); i++){
        cout << dq[i] << " ";
    }
    cout << endl;

    dq.push_front(2);  // [2,1,7,4]
    dq.push_front(5);  // [5,2,1,7,4]
    for(int i = 0; i<dq.size(); i++){
        cout << dq[i] << " ";
    }
    cout << endl;

    dq.pop_front();  // [2,1,7,4]
    dq.pop_back();  // [2,1,7]
    for(int i = 0; i<dq.size(); i++){
        cout << dq[i] << " ";
    }
    cout << endl;

    return 0;
}