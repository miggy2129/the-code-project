## Array 2. Shift an array left.

Input format: `N`, followed by the `N` integers. 

Input:
```
3
3 1 2 
```
Output:
```
1 2 3 
```
Input:
```
5
1 2 3 4 9 
```
Output:
```
2 3 4 9 1 
```
Input:
```
6
3 10 -2 3 1 3 
```
Output:
```
10 -2 3 1 3 3 
```
See [array2.cpp](array2.cpp), [array2.in](array2.in) and [array2.output](array2.output).
