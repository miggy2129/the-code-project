// mat9_readme.cpp
#include <bits/stdc++.h>

using namespace std;
char grid[3][3];

bool checkWin(char c){
    // Check cols
    for(int j = 0; j<3; j++){
        if(grid[0][j] != c) continue;
        bool good = true;
        for(int i = 1; i<3; i++){
            if(grid[i][j] == grid[i-1][j]){
                continue;
            } else {
                good = false;
                break;
            }
        }
        if(good) return true;
    }

    // Check rows
    for(int i = 0; i<3; i++){
        if(grid[i][0] != c) continue;
        bool good = true;
        for(int j = 1; j<3; j++){
            if(grid[i][j] == grid[i][j-1]) {
                continue;
            } else {
                good = false;
                break;
            }
        }
        if(good) return true;
    }

    // Check diagonal (\)
    if(grid[0][0] == c){
        bool good = true;
        for(int i = 1; i<3; i++){
            if(grid[i][i] == grid[i-1][i-1]){
                continue;
            } else {
                good= false;
                break;
            }
        }
        if(good) return true;
    }

    // Check diagonal (/)
    if(grid[0][2] == c){
        bool good = true;
        for(int i = 1; i<3; i++){
            if(grid[i][3-1-i] == grid[i-1][3-1-i+1]){
                continue;
            } else {
                good = false;
                break;
            }
        }
        if(good) return true;
    }

    return false;
}

int main(){
    char name[] = "mat9";
    while(scanf(" %c", &grid[0][0])!=EOF){
        for(int j = 1; j<3; j++) scanf(" %c", &grid[0][j]);
        for(int i = 1; i<3; i++){
            for(int j = 0; j<3; j++){
                scanf(" %c", &grid[i][j]);
            }
        }

        printf("Input:\n```\n");
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                printf("%c", grid[i][j]);
            }
            printf("\n");
        }
        printf("```\n");

        printf("Output:\n```\n");

        if(checkWin('O')){
            printf("O WINS\n");
        } else if (checkWin('X')){
            printf("X WINS\n");
        } else {
            printf("NO WINNER\n");
        }

        printf("```\n");

    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );

    return 0;
}