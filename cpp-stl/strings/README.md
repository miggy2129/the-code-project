## Strings
Unlike in C where strings are just character arrays with a terminating `'\0'`, the [string](http://www.cplusplus.com/reference/string/string/?kw=string) class gives you flexibility in terms of being resizable, various methods like [substr](http://www.cplusplus.com/reference/string/string/substr/), and the convenience of being able to use operators like `+`.

Just like in C, we can go iterate through the characters like this:
```cpp
for(int i = 0; str[i]; i++)
```
Alternatively, we can use [size](http://www.cplusplus.com/reference/string/string/size/) (in place of [strlen](http://www.cplusplus.com/reference/cstring/strlen/?kw=strlen)):
```cpp
for(int i = 0; i<str.size(); i++)
```

We can use [push_back](http://www.cplusplus.com/reference/string/string/push_back/) to append a character (just like in [vectors](../vector)), but for appending strings we can use [append](http://www.cplusplus.com/reference/string/string/append/). Alternatively, we can just use `+` or `+=` to concatenate them (compare this with C's [strcat](http://www.cplusplus.com/reference/cstring/strcat/?kw=strcat)!).

See [strings.cpp](strings.cpp)
```cpp
// strings.cpp
#include <iostream>
#include <string>

using namespace std;

int main(){
    string str = "cat";
    cout << str << endl;

    string other = "dog";

    cout << str << " some text " << other << endl;

    string str3 = str + " and " + other;  // "cat and dog"
    cout << str3 << endl;

    string sample = "My sampl";
    // Use push_back for characters
    sample.push_back('e');  // "My sample"
    cout << sample << endl;

    sample.append(" text.");  // "My sample text."
    cout << sample << endl;

    // We can just use operators!
    sample = sample + " And ";  // "My sample text. And "
    sample += "more.";  // "My sample text. And more."
    cout << sample << endl;

    string sub = sample.substr(14, 9);  // ". And mor"; Staring at index 14, get 9 characters
    cout << sub << endl;

    cout << str + sample.substr(14) << endl;  // "cat. And more."; If we only include the index, substr will include till the end of the string

    for(int i = 0; str[i]; i++){  // Just like in C
        cout << str[i];
    }
    cout << endl;

    for(int i = 0; i<str.size(); i++){  // Similar to strlen in C
        cout << str[i];
    }
    cout << endl;

    return 0;
}
```
Output:
```cpp
cat
cat some text dog
cat and dog
My sample
My sample text.
My sample text. And more.
. And mor
cat. And more.
cat
cat
```