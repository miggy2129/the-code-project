## Breadth-First Search
Used for graph traversal, and also solves the single-source shortest path problem (SSSP).

## Resources
- [Visualgo (Graph Traversal)](https://visualgo.net/dfsbfs)
- [Visualgo (SSSP)](https://visualgo.net/sssp)
- [Wikipedia](https://en.wikipedia.org/wiki/Breadth-first_search)

## Problems
- [Numerical Maze (UVa 868)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&category=24&problem=809&mosmsg=Submission+received+with+ID+18937662). Included in the backtracking part of uHunt, but can be easily solved with BFS.