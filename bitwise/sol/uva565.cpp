// uva565.cpp (UVa565 - Pizza Anyone?)
#include <bits/stdc++.h>

using namespace std;

vector<string> arr;
int N;

int main(){
    string tl;
    while(cin >> tl){
        arr.clear();
        tl.pop_back();
        arr.push_back(tl);

        while(cin >> tl){
            if(tl == ".") break;
            tl.pop_back();
            arr.push_back(tl);
        }

        N = arr.size();
        bool found = false;
        for(int comb = 0; comb<1<<16; comb++){
            bool consistent = true;
            for(int i = 0; i<N; i++){
                bool found_topping = false;
                for(int j = 0; arr[i][j]; j+=2){
                    int bit = arr[i][j] == '+' ? 1 : 0;
                    int idx = arr[i][j+1] - 'A';
                    if((bool) (comb & (1<<idx)) == bit) {
                        found_topping = true;
                        // printf("For comb %d, person %d violated the %d requirement (%d bit %d idx)\n", comb, i, j, bit, idx);
                        break;
                    }
                }
                if(!found_topping){
                    consistent = false;
                    break;
                }
            }
            if(consistent){
                found = true;
                printf("Toppings: ");
                for(int j = 0; j<16; j++){
                    if(comb & (1<<j)){
                        printf("%c", 'A'+j);
                    }
                }
                printf("\n");
                break;
            }
        }

        if(!found){
            printf("No pizza can satisfy these requests.\n");
        }


    }

    return 0;
}
