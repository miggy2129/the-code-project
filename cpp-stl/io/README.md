## i/o redirection
Before starting, it is useful to know about [i/o redirection](http://tldp.org/LDP/abs/html/io-redirection.html). Instead of just running your program and then manually typing on the keyboard every time, you can save your input in a file (say `read.in`) and then just do
```
./mycode.o < read.in
```
Furthermore, we can write (save) our output to a file in a similar fashion. This is useful if your code will give many lines of output that will just fill up your terminal:
```
./mycode.o < read.in > myoutput.txt
```

## cin/cout
For C++, the default way to process i/o is to use `cin` for scanning, and `cout` for printing. See [read-iostream.cpp](read-iostream.cpp) and try it out on [read.in](read.in). The library to be included is `<iostream>`. `cin/cout` can also process the [string](../strings) class (different from C-strings/char arrays!) as shown in the example. See [here](http://www.cplusplus.com/reference/string/string/?kw=string) for the possible operators and methods.

Notice that we have a line `using namespace std`. We do this for convenience, as otherwise we will have to type out `std::cout`, `std::cin`, and `std::endl` instead of just `cout`, `cin`, and `endl`. `endl` (end line) simply prints the `'\n'` character.

While this is convenient, usually it is slower than `scanf/printf`, so those two may be preferred, especially for programming contests.

## scanf/printf
`scanf/printf` make use of [format specifiers](http://www.cplusplus.com/reference/cstdio/printf/), as seen in [read-stdio.cpp](read-stdio.cpp). Note that when scanning, we need to precede the variable with `&`, meaning we are passing the address of the variable for use by `scanf`. The exception is with strings, explained below.

- `%d` - integers. 
- `%c` - char. Usually preceded by whitespace (as in `scanf(" %c", &mychar)`)
- `%f` - float. For scanning doubles, we need to use `%lf`, but can still print using `%f`. See [this](http://stackoverflow.com/questions/4264127/correct-format-specifier-for-double-in-printf). You can also specify the number of decimal places used in printing, like `%.5f` to use 5 decimal places.
- `%s` - strings (char arrays). In C, strings are really just char arrays, with `\0` at the end. As arrays just decompose into [pointers](/pointers) (pointers are a separate topic altogether and will not be covered here; go Google. tl;dr version - pointers are still variables, but instead of storing ints or chars, they store the *address* of other variables instead), we don't need the `&` anymore to get the address of the array. Hence we only do `scanf("%s", mystr)` and not `scanf("%s", &mystr)`.

`cin/cout` and `scanf/printf` are usually in sync, and can be used together (say `scanf` for ints, `cin` for `strings`). Note however that this is usually considered bad practice, and is discouraged especially when doing actual projects.

## Scanning per line
Sometimes, we don't know the format or perhaps even not know how long the input will be (unlike in [read.in](read.in) where we know that there will be two ints, two chars, two floats and then two strings), so in that case we need to process the input line-by-line.

We have [fgets](http://www.cplusplus.com/reference/cstdio/fgets/?kw=fgets) (an improvement over [gets](http://www.cplusplus.com/reference/cstdio/gets/?kw=gets) as it now limits the number of characters to read to prevent overflow) for C and [getline](http://www.cplusplus.com/reference/string/string/getline/?kw=getline) for C++. See [line-fgets.cpp](line-fgets.cpp) and [line-getline.cpp](line-getline.cpp). Note that for `fgets`, it also includes the scanned newline (`'\n'`).

To process the tokens, we can use [stringstream](http://www.cplusplus.com/reference/sstream/stringstream/). [strtok](http://www.cplusplus.com/reference/cstring/strtok/?kw=strtok) also exists for C, but we'd need to work with C-strings again rather than the more convenient `string` class. See [line-fgets-stringstream.cpp](line-fgets-stringstream.cpp) and [line-getline-stringstream.cpp](line-getline-stringstream.cpp).

Alternatively, we may also use the return values of `scanf`/[sscanf](http://www.cplusplus.com/reference/cstdio/sscanf/?kw=sscanf) to determine the number of variables read. Say for example we are expecting 1-3 numbers per line. The return value will tell us how many numbers we have read, and we can act accordingly. See [line-sscanf.cpp](line-sscanf.cpp) and [line-sscanf.in](line-sscanf.in).