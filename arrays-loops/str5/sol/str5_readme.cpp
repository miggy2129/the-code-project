// str5_readme.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str; 
    char name[] = "str5";
    while(getline(cin, str)){
        printf("Input:\n```\n");
        cout << str << endl;
        printf("```\n");
        // Your code here
        char c = str[0];
        int N = str.size();
        for(int i = 0; i<N-1; i++){
            str[i] = str[i+1];
        }
        str[N-1] = c;

        printf("Output:\n```\n");
        cout << str << endl;
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );

    return 0;
}