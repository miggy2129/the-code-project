## Vector
Vectors are simply resizable arrays. This is very convenient, as in C you'd have to deal with [pointers](/pointers) and things like [malloc](http://www.cplusplus.com/reference/cstdlib/malloc/?kw=malloc)/[realloc](http://www.cplusplus.com/reference/cstdlib/realloc/), which can be very messy (i.e. segmentation faults, freeing memory, etc.).

With vectors, we can easily use [resize](http://www.cplusplus.com/reference/vector/vector/resize/), and append elements using [push_back](http://www.cplusplus.com/reference/vector/vector/push_back/). We also have [pop_back](http://www.cplusplus.com/reference/vector/vector/pop_back/), which removes the element at the end. These two operations are done in `O(1)` time, and hence a vector can also be used as a [stack](../stack).

We also have [assign](http://www.cplusplus.com/reference/vector/vector/assign/), which is like `resize` but you can initialize the elements, and also a convenient method [clear](http://www.cplusplus.com/reference/vector/vector/clear/).

See [vector.cpp](vector.cpp).
```cpp
// vector.cpp
#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector<int> arr;  // []; empty
    arr.push_back(3);  // [3]
    arr.push_back(10);  // [3,10]
    arr.push_back(5);  // [3,10,5]
    for(int i = 0; i<arr.size(); i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    arr.pop_back();  // [3,10]
    arr.push_back(7);  // [3,10,7]
    arr.push_back(6);  // [3,10,7,6]
    for(int i = 0; i<arr.size(); i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    arr.clear();  // []; empty
    arr.push_back(1);  // [1]
    arr.push_back(13);  // [1,13]
    for(int i = 0; i<arr.size(); i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    arr.assign(5,7);  // [7,7,7,7,7]; assign 5 elements with value 7
    for(int i = 0; i<arr.size(); i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    arr = vector<int>({5,7,8});  // [5,7,8]; constructing with initializer lists also works
    for(int i = 0; i<arr.size(); i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    vector<int> other({9,10,12});  // [9,10,12]
    for(int i = 0; i<other.size(); i++){
        cout << other[i] << " ";
    }
    cout << endl;

    return 0;
}
```
Output:
```
3 10 5 
3 10 7 6 
1 13 
7 7 7 7 7 
5 7 8 
9 10 12 
```

As [iterators](http://www.cplusplus.com/reference/iterator/) work on all STL containers, we can also use the `auto` keyword:
```cpp
// vector_cpp11.cpp
#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector<string> arr({"cat", "dog", "giraffe"});

    for(int i = 0; i<arr.size(); i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    for(auto itr = arr.begin(); itr!=arr.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    for(auto el : arr){
        cout << el << " ";
    }
    cout << endl;

    return 0;
}
```
Output:
```cpp
cat dog giraffe 
cat dog giraffe 
cat dog giraffe 
```

## References
- [cplusplus.com](http://www.cplusplus.com/reference/vector/vector/?kw=vector)