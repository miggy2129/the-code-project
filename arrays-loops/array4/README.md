## Array 4. Shift even-indexed elements left.

Indices are zero-based.

Input format: `N`, followed by the `N` integers. 

Input:
```
3
3 1 2 
```
Output:
```
2 1 3 
```
Input:
```
5
1 2 3 4 9 
```
Output:
```
3 2 9 4 1 
```
Input:
```
6
3 10 -2 3 1 3 
```
Output:
```
-2 10 1 3 3 3 
```
Input:
```
1
8 
```
Output:
```
8 
```
Input:
```
2
1 2 
```
Output:
```
1 2 
```
Input:
```
4
4 3 2 1 
```
Output:
```
2 3 4 1 
```
Input:
```
7
1 2 3 4 5 6 7 
```
Output:
```
3 2 5 4 7 6 1 
```
See [array4.cpp](array4.cpp), [array4.in](array4.in) and [array4.output](array4.output).
