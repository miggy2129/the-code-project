// str6_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;

    while(getline(cin, str)){
        // Your code here
        int N = str.size();
        char c = str[N-1];
        for(int i = N-1; i>=1; i--){
            str[i] = str[i-1];
        }
        str[0] = c;

        cout << str << endl;
    }

    return 0;
}