// loop8_readme.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int N, M;
    char name[] = "loop8";

    while(scanf("%d %d", &N, &M)!=EOF) {
        printf("Input:\n```\n%d %d\n```\nOutput:\n```\n", N,M);
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("%c", (i+j)%2 ? 'X' : '*');
            }
            printf("\n");
        }
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name);
    return 0;
}