#include <bits/stdc++.h>

using namespace std;

string str = "abc";

int N;

vector<bool> taken;

string build;

void f(int cnt){
    if(cnt == N){
        cout << build << endl;
        return;
    }

    for(int i = 0; i<N; i++){
        if(!taken[i]){
            taken[i] = true;
            build.push_back(str[i]);
            f(cnt+1);
            build.pop_back();
            taken[i] = false;
        }
    }
}

int main(){
    N = str.size();
    taken.assign(N, false);

    f(0);

    return 0;
}