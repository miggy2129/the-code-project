## Philip J. Fry Problem - UVa 12324 (non-classical)

*Note: it is best if you are already familiar with the classical DP problems such as [knapsack](../knapsack) and [LIS](../lis) (if you are not, go study/read about them first). Being a non-classical problem, this walkthrough aims to fill in the gaps by providing a hands-on approach as to what can be changed, and what are similar between problems.*

This problem is similar to [knapsack](../knapsack), in that there are only two choices in deciding which state to go to next.


## Abridged Problem Statement
There are `N` trips which you have to take in order, and each trip takes some time to complete (let's assign <code>t<sub>i</sub></code> to be the number of minutes that the <code>i<sup>th</sup></code> trip will take).

Now for the fun part: at certain parts of the trips, we get to attain some *magical spheres*. These spheres can be used to *cut the duration of a trip in half*. For the <code>i<sup>th</sup></code> trip, we get <code>b<sub>i</sub></code> spheres, and can use these in succeeding (higher indexed, i.e. `i+1` and so on) trips. At each trip, we can either use *one* sphere (if any) and cut its duration in half, or decide to hold on to our spheres and save them for later trips.

The problem then is to minimize the total duration (trip times) for all trips, by using the spheres at the right trips.

For example:
```
T = [40 10 18]
B = [ 1  0  0]
```

That is, we have three trips. The first trip takes 40 minutes, and the second and third trips take 10 and 18 minutes, respectively. Here we get a magical sphere during the first trip, and can use this to halve the duration of either the second or the third trip. The right choice here is to hold on to it and use it on the third trip, for a total of 40 + 10 + 9 = 59 minutes.

Full problem description available [here](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3746).

## Walkthrough
So how do we approach this problem? One method is to use up the spheres as soon as we get them, but as these may be limited, it might be wiser to save them for the right (longer) trips later on. Another approach is to *greedily* pick out the longest trips, but of course this assumes that we have spheres to use for those trips.<sup>1</sup>

An alternative "cheat" solution is to use dynamic programming (DP). I say "cheat" because with DP, we are able to *explore all possible states*, similar to how we can explore all possible board configurations for tic-tac-toe and know that there is only one right first move as the second player.<sup>2</sup>

With [knapsack](../knapsack), we went through the items one by one, and formed two parameters for our state: `index` and `weight_in_bag`. For this problem, we have to go through each trip one by one,<sup>3</sup> and can similarly define two parameters for our state. What are these two parameters? [Answer](sol/philip-j-fry-ans1.md)

Similarly, we also have two actions that we can take at each trip/state. What are those two actions? [Answer](sol/philip-j-fry-ans2.md)

We are now ready to code! We've defined both the states/parameters, as well as the actions that we can use to transition between states. Template code has already been written for you. See [philip-j-fry.cpp](philip-j-fry.cpp).

Take a moment to examine the code. We have separated the two actions (take/not take) into their own blocks/scopes, just for semantics. We define `take_ret` to be the total duration (cost) if we use a magical sphere at the current trip; this includes the halved duration of the current trip, as well as *the optimal solution/duration* of the succeeding trips. Similarly, `skip_ret` would be the cost (total duration) of the current trip (taking its full duration) and the optimal solution to the succeeding trips. Of course, we want to optimize between the two paths, so we take the minimum between them and update `best` accordingly.

Again, just to emphasize, `f(index, spheres_in_hand)`, considers the best solution from that current trip (`index`) towards the last trip (`N-1`); it does not consider/is independent from the decisions taken on earlier trips. Thus, for our state `(index, spheres_in_hand)`, the **subproblem** is defined as the optimal cost (minimum duration) in taking the trips from `index` up to `N-1`, given that we currently have `spheres_in_hand`. `(0,0)` would be the starting state/subproblem, and this is what we call in `main()`.

#### take_ret

For the `take_ret` block, what is the duration spent for the trip if we use a magical sphere? Store this in a variable called `cur_cost`. [Answer](sol/philip-j-fry-ans3.md), [Code](sol/philip-j-fry-ans3.cpp). 

After taking this trip, how do we go to the next state (next index)? Store this value in a variable called `next_index`. [Answer](sol/philip-j-fry-ans4.md), [Code](sol/philip-j-fry-ans4.cpp)

Similarly, what happens to `spheres_in_hand`? Define the next value in `next_spheres_in_hand`. Note that for each trip we get an additional <code>b<sub>i</sub></code> spheres that we can use in succeeding trips. [Answer](sol/philip-j-fry-ans5.md), [Code](sol/philip-j-fry-ans5.cpp)

With `next_index` and `next_spheres_in_hand` ready, define `next_ret` to be the solution to the next state/subproblem. That is, after taking the sphere at the current `index`, what is the optimal solution (minimum time) for the succeeding trips? [Answer](sol/philip-j-fry-ans6.md), [Code](sol/philip-j-fry-ans6.cpp)

Finally, what should the value of `take_ret` be? Again, `take_ret` is the result (cost) of halving the current trip, and getting the optimal result for the subsequent subproblems (future trips). [Answer](sol/philip-j-fry-ans7.md), [Code](sol/philip-j-fry-ans7.cpp)

And we're done! Well, almost. We need to make sure that we have `spheres_in_hand` in the first place. Add in the necessary `if` condition. [Answer](sol/philip-j-fry-ans8.md), [Code](sol/philip-j-fry-ans8.cpp)

We can now proceed to `skip_ret`.

#### skip_ret
Just as with `take_ret`, we just need to define the following variables for the `skip_ret` block:

- `cur_cost` ([Answer](sol/philip-j-fry-ans9.md), [Code](sol/philip-j-fry-ans9.cpp))
- `next_index` ([Answer](sol/philip-j-fry-ans10.md), [Code](sol/philip-j-fry-ans10.cpp))
- `next_spheres_in_hand` ([Answer](sol/philip-j-fry-ans11.md), [Code](sol/philip-j-fry-ans11.cpp))
- `next_ret` ([Answer](sol/philip-j-fry-ans12.md), [Code](sol/philip-j-fry-ans12.cpp)) 
- `skip_ret` ([Answer](sol/philip-j-fry-ans13.md), [Code](sol/philip-j-fry-ans13.cpp)) 

And we're good! However, if you try running your code on the [sample input](philip-j-fry.in), your program will crash. In fact, it will run infinitely! Since we're dealing with recursions, we need to add in our base case.

#### Base case
Recall that for each state `(index, spheres_in_hand)`, we add in the cost for the current `index`, then proceed to get the cost for the next trips (next states). However, what if we don't have any more trips? Which variable will tell us this? [Answer](sol/philip-j-fry-ans14.md)

What will the value of the variable be when there are no more trips (i.e. when we need to stop)? [Answer](sol/philip-j-fry-ans15.md)

What should the return value be when reaching this terminal state? Hint: consider what the (last) state before reaching the terminal state will do. It will add in its current cost, and then add in the cost of the next subproblem (which in this case will be the terminal state). Factor in this base case in your code. [Answer](sol/philip-j-fry-ans16.md), [Code](sol/philip-j-fry-ans16.cpp)

At this point your code should be good. Try running it on the [sample input](philip-j-fry.in) and verify that it produces the [correct output](philip-j-fry.ans). However, if you try to submit it on [UVa](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=submit_problem&problemid=3746&category=24) it will just be judged as Time Limit Exceeded (TLE). This is because we are simply doing regular [backtracking](/backtracking) and not taking advantage of the **overlapping** subproblems of DP. Backtracking takes exponential time, but this can be significantly reduced by [memoization](https://en.wikipedia.org/wiki/Memoization).

#### Memoization
Memoization works because the states (subproblems) are overlapping. This means that if I'm currently trying to solve the subproblem `(index, spheres_in_hand)`, I shouldn't need to solve it again if *the same state has been solved before*. Hence, we can just *cache* our answers.

Take a look at [philip-j-fry-memo.cpp](philip-j-fry-memo.cpp). We have added in two 2D arrays, a boolean `solved` which indicates if a certain state has been solved, and `memo` which contains the actual solution to the subproblem.<sup>4</sup> Modify the code to make use of memoization, and verify that your code is Accepted (AC) on [UVa](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=submit_problem&problemid=3746&category=24).<sup>5</sup> [Answer](sol/philip-j-fry-ans17.md), [Code](sol/philip-j-fry-ans17.cpp)

Congratulations! You've just solved a non-classical DP problem. To further reinforce your learning, proceed to the quiz below, which will just be little variants of this problem.

#### *Notes*

[1] Greedy solution can be found [here](greedy).

[2] The first player selects a corner tile. As the second player, the only correct move is to select the middle tile; we know for sure that all other tiles will lead to a loss, and that this "correct" tile will only lead to a draw. We already know *exactly* how the game ends even at the first move.

[3] This is a property of common DP problems&mdash;the states form a [directed acyclic graph (DAG)](https://en.wikipedia.org/wiki/Directed_acyclic_graph). That is, there are no loops (no cyclic paths) and you can only move from one direction to the other, e.g. from left to right and/or top to bottom; you can't go back to where you were before (a previous state).

[4] Alternatively, we can just have only `memo` and flag unvisited states with a dummy value like `-1` instead of keeping a separate boolean array.

[5] As there are `O(N * max_spheres)` states and no loops for each state evaluation (`O(1)`), the run time of the code is at `O(N * max_spheres)`. Here we can get up to `N` spheres at each trip, so `max_spheres` could be up to <code>N<sup>2</sup></code>, and hence an array size (and overall complexity) of <code>O(N<sup>3</sup>)</code>. However, note that we can cap `max_spheres` at just `N` since it does not yield any benefit to have more spheres than there are trips; we just didn't do so for simplicity.

## Quiz
Random i/o have been generated in [quiz/](quiz). Code your solutions and see if they have the same output as mine. See [quiz/philip-j-fry-quiz.in](quiz/philip-j-fry-quiz.in). For this part, let's assume that `N` can only be up to 20.
#### #1

The problem assumes that the spheres you get in a trip can only be used on succeeding trips, and not on the current trip. Modify your code to make it so that you can use the spheres as soon as you get them. See [quiz/philip-j-fry-quiz1.ans](quiz/philip-j-fry-quiz1.ans).

#### #2
In the original problem, we can only apply a sphere once per trip. Instead, let's make it so that we can use spheres on a trip consecutively, as long as we still have some left. For example, if a trip takes 15 minutes and we have three spheres, we can bring it down to 7 minutes, 3 minutes, and finally 1 minute (note the integer division by 2). Let's retain the original rule that a sphere can only be used in succeeding trips. See [quiz/philip-j-fry-quiz2.ans](quiz/philip-j-fry-quiz2.ans).


#### #3
Do the combination of #1 and #2. That is, you can use spheres consecutively, and use them as soon as you get them. See [quiz/philip-j-fry-quiz3.ans](quiz/philip-j-fry-quiz3.ans).


*Disclaimer: Just as with online judges and programming contests, we can only go so far as to consider code as "Accepted", not "Correct". That is, it yielded the same output for the given sets of input, and so conclude that it is just* ***probably*** *correct.*

## Summary
In this walkthrough we were able to formulate a solution for a non-classical DP problem together, and hopefully have seen some similiarities and differences with classical problems like [knapsack](../knapsack). To recap, here are the steps that we did in formulating our solution:

- **Definition of states/subproblems**. We formulated the parameters that we could use, as well as what exactly the function is returning. This is something that becomes more intuitive through practice.
- **Identification of the possible actions**. Knowing the possible actions to take (e.g. take/not take) led us to our recurrences.
- **Identification of the base case(s)**.
- **Memoization**. This takes advantage of overlapping subproblems and allows our code to run in a reasonable amount of time.

Now that you're done, continue enhancing your DP skill by solving more problems! Go through the classics, and solve **lots** of non-classical problems on different sites ([UVa](http://uva.onlinejudge.org)/[uHunt](http://uhunt.felix-halim.net), [HackerRank](http://hackerrank.com), [Kattis](http://open.kattis.com), etc.). :)

## Problem Link
- [UVa 12324](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3746)

## TODO 
- Visual example for overlapping subproblems.
- (Visual) counter/examples for greedy.
- Code: greedy, process from highest, as long as there are spheres to the left (lower indices), use those spheres (for same cost, process those at the right first, then get the first sphere to the left; would be wrong to get the spheres at the left)