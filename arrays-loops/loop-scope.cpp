// loop-scope.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int i = -7;
    printf("i in main is %d\n", i);
    for(int i = 1; i<=5; i++){
        int myvar = i*3;
        printf("At i=%d, myvar is %d\n", i, myvar);
    }
    printf("i in main is still %d\n", i);

    return 0;
}