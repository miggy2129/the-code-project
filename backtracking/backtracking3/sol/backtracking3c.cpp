#include <bits/stdc++.h>

using namespace std;

string str = "abc";

int N;

string build;

void f(int mask){
    if(mask == ((1<<N)-1)){
        cout << build << endl;
        return;
    }

    for(int i = 0; i<N; i++){
        if(!(mask&(1<<i))){
            build.push_back(str[i]);
            f(mask|(1<<i));
            build.pop_back();
        }
    }
}

int main(){
    N = str.size();
    f(0);

    return 0;
}