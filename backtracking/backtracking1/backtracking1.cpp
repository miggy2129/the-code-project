#include <bits/stdc++.h>

using namespace std;

// No globals, aside from possibly `tabs` in the extra exercise.

void f(int n){
    // Your code here.
}

int main(){
    f(5);  // 5 4 3 2 1 0 1 2 3 4 5

    return 0;
}
