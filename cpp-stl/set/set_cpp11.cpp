// set_cpp11.cpp
#include <iostream>
#include <set>

using namespace std;

int main(){
    set<int> s({1,3,5});

    for(auto itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    for(auto el : s){
        cout << el << " ";
    }
    cout << endl;

    // Erase an element using iterators
    auto it = s.find(3);
    if(it != s.end()){  // element exists
        s.erase(it);
    }
    // {1,5}
    for(auto el : s){
        cout << el << " ";
    }
    cout << endl;


    return 0;
}