// loop-scope-wrong.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    for(int i = 1; i<=5; i++){
        int myvar = i*3;
        printf("At i=%d, myvar is %d\n", i, myvar);
    }
    printf("i=%d\n", i);  // line 11
    printf("myvar=%d\n", myvar);  // line 12

    return 0;
}