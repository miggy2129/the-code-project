## Map
With [vectors](../vector) we can access elements using `[]`, but are limited to integer indices. Maps extend this functionality by associating any kind of key to any kind of value; if you're familiar with Python then this would be the equivalent of [dictionaries](https://docs.python.org/2/tutorial/datastructures.html#dictionaries).

We assign key-value pairs the usual way, like `mymap["dog"] = 13;`. To check if a key exists, we can use [find](http://www.cplusplus.com/reference/map/map/find/). Just like with [sets](../set), the key-value pairs are automatically sorted. As their implementations are similar, these operations are done in `O(log n)`. That said, we might want to use [unordered_maps](http://www.cplusplus.com/reference/unordered_map/unordered_map/?kw=unordered_map) (introduced in C++11), which use hashes to improve performance.

See [map.cpp](map.cpp).
```cpp
// map.cpp
#include <iostream>
#include <map>

using namespace std;

int main(){
    map<string, int> freq;  // Let's say we want to keep track of the number of animals we see.
    freq["dog"] = 5;
    freq["cat"] = 3;
    freq["giraffe"] = 2;

    cout << "We have " << freq.size() << " entries" << endl;
    for(map<string,int>::iterator itr = freq.begin(); itr!=freq.end(); itr++){  // Notice how we type so much stuff because of iterators; this is so much cleaner if we use 'auto'
        cout << itr->first << ":" << itr->second << endl;  // key-value pairs are automatically sorted
    }

    cout << freq["not found"] << endl;  // If we try to access elements that are not in the map, a key-value pair is inserted (the default constructors are used)
    cout << "We now have " << freq.size() << " entries because of that" << endl;
    for(map<string,int>::iterator itr = freq.begin(); itr!=freq.end(); itr++){ 
        cout << itr->first << ":" << itr->second << endl;
    }

    // A cleaner way is to use find(), which does not insert a new element 
    if(freq.find("owl") == freq.end()){  // Not found
        cout << "We did not see any owls" << endl;
    }
    cout << "Freq size is still " << freq.size() << endl;

    return 0;
}
```
Output:
```
We have 3 entries
cat:3
dog:5
giraffe:2
0
We now have 4 entries because of that
cat:3
dog:5
giraffe:2
not found:0
We did not see any owls
Freq size is still 4
```
With C++11, we can use `unordered_maps` as well as the `auto` keyword:
```cpp
// map_cpp11.cpp
#include <iostream>
#include <unordered_map>

using namespace std;

int main(){
    unordered_map<string, int> freq;  // Not sorted, uses hashes to be faster
    freq["dog"] = 5;
    freq["cat"] = 3;
    freq["giraffe"] = 2;

    for(auto kv : freq){  // Much cleaner, and we don't need to change anything if we change the map type.
        cout << kv.first << ":" << kv.second << endl;
    }
    return 0;
}
```
Output:
```
giraffe:2
cat:3
dog:5
```

## References
- [Map](http://www.cplusplus.com/reference/map/map/?kw=map)
- 