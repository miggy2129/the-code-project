*Note: At the very least you should know about the concept of [recursion](../recursion) (e.g. doing factorial without loops)*.

Before going into [dynamic programming](../dynamic-programming) (DP), we need to learn backtracking first. Check out the **fundamental** problems (1-3, especially 1) in that order, and then finally problem 4 (my solution uses [STL](/cpp-stl)). Ideally you don't look at the solutions in each, but for problem 1 maybe you might after trying to internalize the hints given but are still stuck (this is the key concept that isn't emphasized in programming courses). Try to solve the other problems on your own after that. If ever you do look at the solutions, make sure you try to code them on your own after some time without looking.

After going through these, try improving your skill by studying [dynamic programming (DP)](/dynamic-programming) problems. Also try to solve the problems in Ch. 3 of uHunt.

## Additional Exercises

- [Printing all toposorts](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=813). There are two methods to do a toposort: one involves running a depth-first search (DFS), and the other invovles keeping a count of incoming edges. Google `topological sort` for the algorithms; there is a section in Ch 4 (Graphs) in uHunt just for [toposorts](/competitive-programming/graphs/toposort).
- [Jugs (UVa 571)](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=512)
- [Graph Coloring (UVa 193)](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=129). Common graph coloring problem (binary).
- [Coloring Graphs (Kattis)](https://open.kattis.com/problems/coloring). Graph coloring again (multiple colors, small graph). 
- [Hidden Plus Signs (UVa 12872)](https://uva.onlinejudge.org/external/128/12872.pdf). Interesting problem. Taken from [ACM-ICPC Bangkok Regionals 2014](https://www.acm-icpc.eng.chula.ac.th/acm-2014/index.php).
- [Password (UVa 1262)](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3703). Similar to [backtracking2](backtracking2).
