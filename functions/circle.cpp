// circle.cpp
#include <bits/stdc++.h>

using namespace std;

#define PI 3.14

int main(){
    double r = 2.5;
    double area = PI*r*r;
    printf("The area of the circle with radius %f is %f\n", r, area);
    return 0;
}