// UVa 1096 - The Islands (Ch. 9 of uHunt - Bitonic TSP)
#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> pi;

#define sqr(x) ((x)*(x))
#define INF (270000000)

int N;

pi input[100];
bool isSpecial[100];

bool vis[110][110][3];
double arr[110][110][3];

double dist(pi a, pi b){
    return sqrt(sqr(a.first-b.first) + sqr(a.second-b.second));
}

vector<int> build1, build2;

double f(int lastA, int lastB, int mask){

    if(vis[lastA+1][lastB+1][mask]){
        return arr[lastA+1][lastB+1][mask];
    }

    int index = max(lastA, lastB) + 1;
    if(index == N-1){
        if(mask != 3) return INF;
        return dist(input[index], input[lastA])  +dist(input[index], input[lastB]);
    }

    if(index == 0){
        return f(index, index, 0);
    }

    double best = INF;
    if(isSpecial[index]){
        // Try putting in first bit 
        if(!(mask & (1<<0))) {
            double this_ret = dist(input[lastA], input[index]) + f(index, lastB, mask | (1<<0));
            if(this_ret < best){
                best = this_ret;
            }
        }

        // Try putting in second bit
        if(!(mask & (1<<1))) {
            double this_ret = dist(input[lastB], input[index]) + f(lastA, index, mask | (1<<1));
            if(this_ret < best) {
                best = this_ret;
            }
        }
    } else {
        // Try putting in first path
        {
            double this_ret = dist(input[lastA], input[index]) + f(index, lastB, mask);
            if(this_ret < best ){
                best = this_ret;
            }
        }

        // Second path
        {
            double this_ret = dist(input[lastB], input[index]) + f(lastA, index, mask);
            if(this_ret < best ){
                best = this_ret;
            }
        }
    }


    vis[lastA+1][lastB+1][mask] = 1;
    arr[lastA+1][lastB+1][mask] = best;
    return best;
}

void printPath(int lastA, int lastB, int mask){

    double bestAns = f(lastA, lastB, mask);

 
    int index = max(lastA, lastB) + 1;
    if(index == N-1){
        build1.push_back(N-1);
        build2.push_back(N-1);
        return;
    }

    if(index == 0){
        build1.push_back(0);
        build2.push_back(0);
        printPath(index,index,0);
        return;
    }

    double best = INF;
    if(isSpecial[index]){
        // Try putting in first bit 
        if(!(mask & (1<<0))) {
            double this_ret = dist(input[lastA], input[index]) + f(index, lastB, mask | (1<<0));
            if(this_ret == bestAns){
                build1.push_back(index);
                printPath(index, lastB, mask | (1<<0));
                return;
            }
        }

        // Try putting in second bit
        if(!(mask & (1<<1))) {
            double this_ret = dist(input[lastB], input[index]) + f(lastA, index, mask | (1<<1));
            if(this_ret == bestAns){
                build2.push_back(index);
                printPath(lastA, index, mask | (1<<1));
                return;
            }
        }
    } else {
        // Try putting in first path
        {
            double this_ret = dist(input[lastA], input[index]) + f(index, lastB, mask);
            if(this_ret == bestAns){
                build1.push_back(index);
                printPath(index, lastB, mask);
                return;
            }
        }

        // Second path
        {
            double this_ret = dist(input[lastB], input[index]) + f(lastA, index, mask);
            if(this_ret == bestAns){
                build2.push_back(index);
                printPath(lastA, index, mask);
                return;
            }
        }
    }

}
int main(){
    int cn = 0;
    while(scanf("%d", &N)!=EOF){
        if(!N) break;
        cn++;
        memset(vis, 0, sizeof vis);
        memset(isSpecial, 0, sizeof isSpecial);
        build1.clear();
        build2.clear();
        int b1, b2;
        scanf("%d %d", &b1, &b2);
        isSpecial[b1] = 1;
        isSpecial[b2] = 1;
        for(int i = 0; i<N; i++){
            scanf("%d %d", &input[i].first, &input[i].second);
        }

        double ans = f(-1,-1,0);
        printf("Case %d: %.2f\n",cn, ans);
        printPath(-1,-1,0);
        for(int i = 0; i<build1.size(); i++){
            if(i) printf(" ");
            printf("%d" ,build1[i]);
        }
        for(int i = build2.size()-2; i>=0; i--){
            printf(" %d", build2[i]);
        }
        printf("\n");
    }

    return 0;
}