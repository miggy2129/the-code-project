#include <bits/stdc++.h>

using namespace std;

vector<string> arr({"abc", "de", "fgh"});

int N;

void f(int index, string build){
    if(index == N){
        cout << build << endl;
        return;
    }

    for(int j = 0; j<arr[index].size(); j++){
        string next_build = build;
        next_build.push_back(arr[index][j]);
        f(index+1, next_build);
    }
}

int main(){
    N = arr.size();
    f(0, "");
}
