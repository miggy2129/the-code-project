// philip-j-fry-ans-4.cpp (UVa 12324 - Philip J. Fry Problem)
#include <bits/stdc++.h>

using namespace std;

#define INF (1<<28)

int N;
int input_time[100];  // Take note that there is a specified ordering (you go from trip 0 to trip N-1 in order) hence forming a directed acyclic graph (DAG), which is usually what you have for DP problems

int input_sphere[100];

/*
    Index refers to what we want to do with the current trip, whether to (1) take it as it is (in hopes of using the spheres for longter trips later), or (2) to use the spheres that we have (if any) to reduce the total cost.
    Index is zero-based for convenience (i.e. index=0 for the first trip).

    spheres_in_hand refers to the spheres that we can use to halve the duration for the current index.
    Do note that if we get some spheres at the current index, we can only use those for later trips (higher indices).
*/
int f(int index, int spheres_in_hand) {

    

    int best = INF;

    /*  
        We separate the two actions that we can take into two different scopes/blocks just for semantics. This isn't necessary, but I think it helps get the point across.
    */

    // "Take" or use the magical sphere to halve the duration of the current trip.
    {
        int take_ret;
        
        /*
            We are using a magical sphere, so the duration of the current trip is halved.
        */
        int cur_cost = input_time[index]/2;

        /*
            We just need to go to the next trip.
        */
        int next_index = index+1;

        // ...

        best = min(best, take_ret);
    }

    // "Skip" using the magical sphere, and save it for later.
    {
        int skip_ret;

        // ...

        best = min(best, skip_ret);
    }


    return best;
}

int main(){
    while(scanf("%d", &N)!=EOF){
        if(!N) break;

        for(int i = 0; i<N; i++){
            scanf("%d %d", &input_time[i], &input_sphere[i]);
        }

        int ans = f(0,0);  // Start from the first trip (index 0), with 0 spheres in hand.
        printf("%d\n", ans);

    }

    return 0;
}