## Loop 7. Print an NxM board with alternating vertical stripes.

The first column would contain `'*'`, the second `'X'`, the third `'*'`, and so on.

Input:
```
3 2
```
Output:
```
*X
*X
*X
```
Input:
```
4 5
```
Output:
```
*X*X*
*X*X*
*X*X*
*X*X*
```
Input:
```
9 3
```
Output:
```
*X*
*X*
*X*
*X*
*X*
*X*
*X*
*X*
*X*
```
Input:
```
1 8
```
Output:
```
*X*X*X*X
```
Input:
```
2 2
```
Output:
```
*X
*X
```
See [loop7.cpp](loop7.cpp), [loop7.in](loop7.in) and [loop7.output](loop7.output).
