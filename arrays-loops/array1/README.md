## Array 1. Read in and print an array of integers.

Input format: `N`, followed by the `N` integers.

*Hint: Remember that we use `&` (address of) when using `scanf`.*

Input:
```
3
3 1 2 
```
Output:
```
3 1 2 
```
Input:
```
5
1 2 3 4 9 
```
Output:
```
1 2 3 4 9 
```
Input:
```
6
3 10 -2 3 1 3 
```
Output:
```
3 10 -2 3 1 3 
```
See [array1.cpp](array1.cpp), [array1.in](array1.in) and [array1.output](array1.output).
