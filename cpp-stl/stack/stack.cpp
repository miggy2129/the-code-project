// stack.cpp
#include <stack>
#include <iostream>

using namespace std;

int main(){
    stack<int> st;  // []; empty stack

    st.push(5);  // [5]
    st.push(17);  // [17,5]
    st.push(16);  // [16,17,5]
    st.push(35);  // [35,16,17,5]
    st.push(3);  // [2,35,16,17,5]

    cout << "The stack has " << st.size() << " items." << endl;  // 5 elements
    int item = st.top();  // 2; does not remove from the stack
    cout << "The item at the top is " << item << endl;
    cout << "There are still " << st.size() << " items in the stack" << endl;  // still 5 elements

    st.pop();  // [35,16,17,5]
    cout << "The top of the stack is now " << st.top() << endl;  // 35
    cout << "The stack now has " <<  st.size() << " elements" << endl;  // 4 elements
    
    st.push(41);  // [41,35,16,17,5]
    cout << "Printing:";
    while(!st.empty()){
        cout << " " << st.top();
        st.pop();
    }
    cout << endl;
    cout << "The stack is now empty with " << st.size() << " elements" << endl;

    return 0;
}