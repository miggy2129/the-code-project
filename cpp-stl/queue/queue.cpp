// queue.cpp
#include <queue>
#include <iostream>

using namespace std;

int main(){
    queue<int> que;  // [] ; queue is empty

    que.push(1);  // [1]
    que.push(10);  // [1,10]
    que.push(5);  // [1,10,5]
    que.push(6);  // [1,10,5,6]

    cout << "Size is " << que.size() << endl;  // 4 elements in queue
    
    int item = que.front();  // gets the item at the front of the queue, which is 1
    cout << "Front: " << item << endl;
    cout << "Size is " << que.size() << endl;  // Calling front() doesn't remove the item at the front...

    que.pop();  // ...but pop() does; [10,5,6]
    cout << "Size after popping is  " << que.size() << endl;  // Now only 3 elements
    cout << "Front is now " << que.front() << endl;  // 10

    cout << "Going through the queue..." << endl;
    while(!que.empty()){
        int item = que.front();
        que.pop();
        cout << "Popped " << item << endl;
    }

    cout << "Queue size is now " << que.size() << endl;


    return 0;
}