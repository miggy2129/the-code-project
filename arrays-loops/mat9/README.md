## Mat 9. Read in a tic-tac-toe board and output the winner.
Input format: 3 lines of 3 characters each, either `'O'`, `'X'` or `'.'`.

Output either `O WINS`, `X WINS` or `NO WINNER`. Assume that the input is valid.

Input:
```
O.X
.O.
X.O
```
Output:
```
O WINS
```
Input:
```
XXX
OO.
...
```
Output:
```
X WINS
```
Input:
```
.OX
O.X
.O.
```
Output:
```
NO WINNER
```
Input:
```
X.X
O.O
O.X
```
Output:
```
NO WINNER
```
Input:
```
X.O
X.O
.XO
```
Output:
```
O WINS
```
Input:
```
O.X
O.X
O..
```
Output:
```
O WINS
```
Input:
```
.OX
.OX
.O.
```
Output:
```
O WINS
```
Input:
```
X.O
X.O
..O
```
Output:
```
O WINS
```
Input:
```
XXX
OO.
...
```
Output:
```
X WINS
```
Input:
```
O..
XXX
.OO
```
Output:
```
X WINS
```
Input:
```
O.O
..O
XXX
```
Output:
```
X WINS
```
Input:
```
O.X
O.X
X..
```
Output:
```
NO WINNER
```
Input:
```
O.X
.O.
...
```
Output:
```
NO WINNER
```
Input:
```
XOX
OXO
X..
```
Output:
```
X WINS
```
See [mat9.cpp](mat9.cpp), [mat9.in](mat9.in) and [mat9.output](mat9.output).
