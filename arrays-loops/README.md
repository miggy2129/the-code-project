
## Exercises
Some of the exercises here ask for user input. As mentioned [here](/cpp-stl/io), you can make use of input redirection like so:
```
./mycode.o < exercise.in
```
**Loops**
- [loop1](loop1). Different variations of looping.
- [loop2](loop2). Print an NxM grid.
- [loop3](loop3). Print a right triangle with base N.
- [loop4](loop4). Print a right triangle again, pointing the other way.
- [loop5](loop5). Print a diamond with an odd N at the middle.
- [loop6](loop6). Print an NxM board with alternating horiontal stripes.
- [loop7](loop7). Print an NxM board with alternating vertical stripes.
- [loop8](loop8). Print a checkered/alternating NxM board.

**Arrays**
- [array1](array1). Read in and print an array of integers.
- [array2](array2). Shift an array left.
- [array3](array3). Shift an array right.
- [array4](array4). Shift even-indexed elements left.
- [array5](array5). Shift odd-indexed elements right.

**2D-Arrays/Matrices/Grids**
- [mat1](mat1). Read in and print a character grid.
- [mat2](mat2). Vertical flip.
- [mat3](mat3). Horizontal flip.
- [mat4](mat4). Diagonal flip (transpose). (\\)
- [mat5](mat5). Diagonal flip. (/)
- [mat6](mat6). Rotate clockwise.
- [mat7](mat7). Rotate counterclockwise. 
- [mat8](mat8). Perform matrix multiplication.
- [mat9](mat9). Read in a tic-tac-toe board and output the winner.
- [mat10](mat10). Read in a sudoku board and output if valid/invalid.

**Strings**
- [str1](str1). Turn a string uppercase. (Hint: [toupper](http://www.cplusplus.com/reference/cctype/toupper/?kw=toupper))
- [str2](str2). Manually check a string for substrings. (No using [substr](http://www.cplusplus.com/reference/string/string/substr/).)
- [str3](str3). Check if a string is a palindrome.
- [str4](str4). Reverse a string.
- [str5](str5). Shift a string left. (No using [substr](http://www.cplusplus.com/reference/string/string/substr/).)
- [str6](str6). Shift a string right. (No using [substr](http://www.cplusplus.com/reference/string/string/substr/).)


## Problems
- [Goldleaf (Kattis)](https://open.kattis.com/problems/goldleaf). 2D array (grid) manipulation.
- [Peragram (Kattis)](https://open.kattis.com/problems/peragrams). Related to anagrams.

## Loops
[Loops](http://www.cplusplus.com/doc/tutorial/control/) make it easy to do things repeatedly. For instance, say we wanted to print `'x'` five times. We could print it manually, but what if we needed to do so 1000 times? Hence, we need to use loops, just like this:

```cpp
int i;
for(i = 1; i<=5; i++){
    printf("x");
}
```
Output:
```
xxxxx
```
There are three parts (four including the body) to this loop:
```cpp
for(initialization; condition; post-increment){
    // loop body
}
```
At the very start, we execute the *initialization* **once**. Here we just set the variable `i` to 1.
```cpp
i = 1.
```
Then, the actual loop part begins. Each time we try to enter the loop, we check the *condition*. If the condition is true, we execute the loop body, followed by the *post-increment*. If the condition is false, we do nothing (we exit the loop).

Simulating:

```cpp
i = 1. i is <= 5, so we print 'x'. Post-increment i to 2.
i = 2. i is <= 5, so we print 'x'. Post-increment i to 3.
i = 3. i is <= 5, so we print 'x'. Post-increment i to 4.
i = 4. i is <= 5, so we print 'x'. Post-increment i to 5.
i = 5. i is <= 5, so we print 'x'. Post-increment i to 6.
i = 6. i is NOT <= 5, so we exit. We don't execute the loop body nor do another post-increment, and i remains at 6.
```

[Full code](loop.cpp):
```cpp
// loop.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int i;
    for(i = 1; i<=5; i++){
        printf("i=%d. Executing the body.\n", i);
    }
    printf("The final value of i is %d.\n", i);

    return 0;
}
```
Output:
```cpp
i=1. Executing the body.
i=2. Executing the body.
i=3. Executing the body.
i=4. Executing the body.
i=5. Executing the body.
The final value of i is 6.
```
Note that at the last part when the condition is false, `i` is 6 and remains that way when the loop exits. 

#### Scope

Also, note that a loop has its own [scope](https://www.tutorialspoint.com/cplusplus/cpp_variable_scope.htm). In C++, we can actually declare the initializers within the loop, as in 
```cpp
for(int i = 1; i<=5; i++)
```
and this `i` will only exist for that loop.

The following [code](loop-scope-wrong.cpp) won't compile, as the variables `i` and `myvar` only exist *within* the loop, but don't exist outside in `main`.
```cpp
// loop-scope-wrong.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    for(int i = 1; i<=5; i++){
        int myvar = i*3;
        printf("At i=%d, myvar is %d\n", i, myvar);
    }
    printf("i=%d\n", i);  // line 11
    printf("myvar=%d\n", myvar);  // line 12

    return 0;
}
```
Compiler output:
```
% g++ loop-scope-wrong.cpp -o loop-scope-wrong.o
loop-scope-wrong.cpp: In function ‘int main()’:
loop-scope-wrong.cpp:11:22: error: ‘i’ was not declared in this scope
     printf("i=%d\n", i);  // line 11
                      ^
loop-scope-wrong.cpp:12:26: error: ‘myvar’ was not declared in this scope
     printf("myvar=%d\n", myvar);  // line 12
```
This is made clearer in [this example](loop-scope.cpp):
```cpp
// loop-scope.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int i = -7;
    printf("i in main is %d\n", i);
    for(int i = 1; i<=5; i++){
        int myvar = i*3;
        printf("At i=%d, myvar is %d\n", i, myvar);
    }
    printf("i in main is still %d\n", i);

    return 0;
}
```
Output:
```cpp
i in main is -7
At i=1, myvar is 3
At i=2, myvar is 6
At i=3, myvar is 9
At i=4, myvar is 12
At i=5, myvar is 15
i in main is still -7
```
Since we gave (declared) our loop its own `i`, it uses its own and leaves `main`'s `i` untouched.
There are also other examples in [functions](/functions), so go ahead and check them out.

#### Other examples
Usually, the examples given for `for` loops would start with `i=0`, and use `<` instead of `<=`. This is because array indexing starts at 0 (because of [pointers](/pointers), but that's another topic), and so it is what is usually used.

Rewriting the example from above, we have [this](loop-0.cpp):
```cpp
// loop-0.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int cnt = 0;
    int i;
    for(i = 0; i<5; i++){
        printf("i=%d. Executing the body.\n", i);
        cnt++;
    }
    printf("The final value of i is %d.\n", i);
    printf("Loop executed %d times.\n", cnt);

    return 0;
}
```
Output:
```cpp
i=0. Executing the body.
i=1. Executing the body.
i=2. Executing the body.
i=3. Executing the body.
i=4. Executing the body.
The final value of i is 5.
Loop executed 5 times.
```
Here, we start with `i = 0`, and end with `i = 4`. At `i = 5`, `i` is NOT `< 5`, so we exit the loop. Before, we counted 1,...,5. Here, we count 0,...,4, and this still gives us a total of 5 iterations just like before.

Also, note that we can use multiple statements in the `for` loop. See this [example](loop-multiple.cpp):
```cpp
// loop-multiple.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    for(int i = 1, j = 2; i<=5; i=i+1, j*=2){
        printf("i = %d, j = %d\n", i,j);
    }

    return 0;
}
```
Output:
```cpp
i = 1, j = 2
i = 2, j = 4
i = 3, j = 8
i = 4, j = 16
i = 5, j = 32
```

Lastly, we mention that there are also other kinds of loops, namely `while` and `do-while`. These can be found in the [tutorial](http://www.cplusplus.com/doc/tutorial/control/) linked below.


## If-else
`If-else` statements let us use *conditions* to control program execution. For example:
```cpp
int age = 14;
if (age >= 13 && age <= 19) {
    printf("You're a teenager!\n");
} else {
    printf("You are not a teenager.\n");
}
```
Output:
```cpp
You're a teenager!
```
Note that with `if-else`, if one of the branches evaluates true, the others aren't executed any more. For example, compare this:
```cpp
int a = 3;
int b = 10;
int c = 7;
if (a > 0) {
    printf("a is %d\n", a);
} else if (b > 0) {
    printf("b is %d\n", b);
} else if (c > 0) {
    printf("c is %d\n", c);
}
```
Output:
```cpp
a is 3
```
with this:
```cpp
int a = 3;
int b = 10;
int c = 7;
if (a > 0) {
    printf("a is %d\n", a);
} 
if (b > 0) {
    printf("b is %d\n", b);
}
if (c > 0) {
    printf("c is %d\n", c);
}

```
Output:
```cpp
a is 3
b is 10
c is 7
```

To make things more compact, we can use ternary operators:
```cpp
    int a = 3;
    int positive = a > 0 ? 1 : 0;  // (condition) ? (value if true) : (value if false)
    printf("%d\n", positive);
```
Output:
```cpp
1
```
For Python, it is even more semantic:
```python
    a = 3
    positive = 1 if a > 0 else 0
    print(positive)
```
Output:
```python
1
```
These are summarized in [if.cpp](if.cpp) and [if.py](if.py).

Finally, there are also `switch (case)` statements, which can be looked up in the same [tutorial](http://www.cplusplus.com/doc/tutorial/control/) below.

## Arrays
An array is simply a contiguous collection of elements:
```cpp
int arr[10];  // uninitialized array of 10 elements
int other[] = {4,8,10};  // the size is determined if declared like this

for(int i = 0; i<3; i++){
    printf("%d ", other[i]);
}
```
Output:
```
4 8 10
```
Members are accessed using `[]`. Arrays are 0-based (i.e. the first element is `[0]`). This is because the indices are *offsets* with respect to the start of the array (see [pointers](/pointers)). Make sure that the indices must be within the proper bounds (i.e. from 0 to N-1 for an N-sized array), or else either (1) you get unintended behavior, or (2) the program crashes (reports "segmentation fault") as in the [code](array-segfault.cpp) below:
```cpp
// array-segfault.cpp
#include <bits/stdc++.h>

using namespace std;

int arr[3];
int main(){
    arr[-200] = 12;  // segmentation fault!
    return 0;
}
```
Output:
```
% ./array-segfault.o
zsh: segmentation fault (core dumped)  ./array-segfault.o
```

We can also have multidimensional arrays, as in the case of grids/matrices and C-strings (char arrays terminated by `'\0'`).

See [array.cpp](array.cpp) for the full code.
```cpp
// array.cpp
#include <bits/stdc++.h>

using namespace std;

int zeroed[10];  // global arrays are zeroed by default

int main(){
    int arr[10];  // uniniitalized, and might contain random data
    int arr_zero[10] = {};  // or zero it this way
    int other[] = {4,8,10};  // size determined from initialization

    for(int i = 0; i<10; i++){
        printf("%d ", zeroed[i]);
    }
    printf("\n");

    for(int i = 0; i<10; i++){
        printf("%d ", arr[i]);
    }
    printf("\n");

    for(int i = 0; i<10; i++){
        printf("%d ", arr_zero[i]);
    }
    printf("\n");

    for(int i = 0; i<3; i++){
        printf("%d ", other[i]);
    }
    printf("\n");

    char grid[3][20] = {"cat", "dog", "giraffe"};  // 3 strings, each up to 20 characters (including the terminating '\0')
    for(int i = 0; i<3; i++){
        for(int j = 0; grid[i][j]; j++){
            printf("%c", grid[i][j]);
        }
        printf("\n");
    }
    // Or simply:
    for(int i = 0; i<3; i++){
        printf("%s\n", grid[i]);
    }

    return 0;
}
```
Output:
```
0 0 0 0 0 0 0 0 0 0 
709833024 32767 4196769 0 2 0 4196861 0 142 0 
0 0 0 0 0 0 0 0 0 0 
4 8 10 
cat
dog
giraffe
cat
dog
giraffe
```

Array-sizes have to be fixed at compile time. For resizable arrays, see [vectors](/cpp-stl/vector).

## Equivalence of char and int
While integers are typically 4 bytes long (32 bits), characters are only a byte (8 bits) big, and range from 0 to 2<sup>8</sup>-1 = 255. If you look at this [ASCII table](http://www.asciitable.com/), you'll see that `'A'` is the same as 65, and that 122 is the same as `'z'`. Hence, we can print integers as characters, and even add and compare between characters and integers.

For example:
```cpp
for(int c = 'd'; c<='g'; c++){
    printf("%d %c\n", c, c);
}
```
Output:
```
100 d
101 e
102 f
103 g
```

This is especially handy when trying to use character [maps](/cpp-stl/map) (say we want to count the number of occurences of each character in a string). We can just use arrays (or [vectors](/cpp-stl/vector)) and have the same "hashing"/"mapping" functionality, but in `O(1)` instead of `O(log n)`.

```cpp
string str = "aaagggzz";
int freq[26] = {};  // initialize to zero
for(int i = 0; str[i]; i++){
    char c = str[i];
    freq[c-'a']++;  // assuming the string is all lowercase
}
```
Or alternatively,
```cpp
string str = "aaagggzz";
int freq[256] = {};  // chars are 8 bits wide, although ASCII characters only go until 127
for(int i = 0; str[i]; i++){
    char c = str[i];
    freq[c]++;  // assuming the string is all lowercase
}
```

See [char-int.cpp](char-int.cpp) for the full code.
```cpp
// char-int.cpp
#include <bits/stdc++.h>

using namespace std;

int arr[256];  // zeroed by default

int main(){
    for(int c = 'd'; c <= 'g'; c++){
        printf("%d %c\n", c, c);
    }

    int freq[26] = {};  // smaller size

    string str = "aaagggzz";

    for(int i = 0; str[i]; i++){
        char c = str[i];
        freq[c-'a']++;  // assuming the string is all lowercase
    }

    // Or alternatively
    for(int i = 0; str[i]; i++){
        arr[str[i]]++;
    }

    for(int c = 'a'; c <= 'z'; c++){
        if(freq[c-'a']){
            printf("%c:%d\n", c, freq[c-'a']);
        }
    }

    for(char c = 'a'; c <= 'z'; c++){
        if(arr[c]){
            printf("%c:%d\n", c, arr[c]);
        }
    }

    return 0;
}
```
Output:
```
100 d
101 e
102 f
103 g
a:3
g:3
z:2
a:3
g:3
z:2
```

## References
- [cplusplus.com](http://www.cplusplus.com/doc/tutorial/control/). Loops, if, switch.


