// philip-j-fry-greedy.cpp
#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> pi;

int input_time[100];
int input_sphere[100];

int N;
int main(){
    while(scanf("%d", &N)!=EOF){
        if(!N) break;
        priority_queue<pi> pq; // sort by longest trip, then rightmost index
        for(int i = 0; i<N; i++){
            scanf("%d %d", &input_time[i], &input_sphere[i]);
            pq.push(pi(input_time[i], i));
        }

        int cnt = 0;
        while(!pq.empty()){
            pi p = pq.top();
            pq.pop();

            int dur = p.first;
            int idx = p.second;

            bool found_sph = false;
            for(int i = idx-1; i>=0; i--){
                if(input_sphere[i]){
                    input_sphere[i]--;
                    found_sph = true;
                    break;
                }
            }

            if(found_sph){
                cnt += dur/2;
            } else {
                cnt += dur;
            }
        }

        printf("%d\n", cnt);


    }

    return 0;
}