// mat6_sol.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int arr[MAX_N][MAX_N];
    int N;
    int arr2[MAX_N][MAX_N];  // My solution writes it to another array

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                scanf("%d", &arr[i][j]);
            }
        }

        // Your code here
        for(int j = 0; j<N; j++){
            for(int i = 0; i<N; i++){
                arr2[j][N-1-i] = arr[i][j];
            }
        }

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                printf("%d ", arr2[i][j]);
            }
            printf("\n");
        }
    }

}