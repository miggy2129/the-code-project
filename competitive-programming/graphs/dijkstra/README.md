## Dijkstra's 
[Dijkstra's](https://en.wikipedia.org/wiki/Dijkstra's_algorithm) algorithm solves the single-source shortest path problem, assuming that there are no negative weights. You could think of it as similar to [Breadth-First Search (BFS)](../bfs), exploring all nodes with increasing total distance from the source. That said, whereas BFS uses a [queue](/cpp-stl/queue), an implementation of Dijkstra's would use a [priority queue](/cpp-stl/priority-queue). 

## Resources
- [Wikipedia](https://en.wikipedia.org/wiki/Dijkstra's_algorithm)
- [Geeks for Geeks](http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/)
- [Visualgo](https://visualgo.net/sssp)
