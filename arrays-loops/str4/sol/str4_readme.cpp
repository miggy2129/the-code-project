// str4_readme.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;

    char name[] = "str4";
    while(getline(cin, str)){
        // Your code here
        printf("Input:\n```\n");
        cout << str << endl;
        int N = str.size();
        for(int i = 0; i<N/2; i++){
            swap(str[i], str[N-1-i]);
        }
        printf("```\nOutput:\n```\n");
        cout << str << endl;
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );

    return 0;
}