// UVa 1262.cpp
#include <bits/stdc++.h>

using namespace std;

int K;
string build;
set<string> dump;

char input1[6][10];
char input2[6][10];

int findChar(char c, int col){
    for(int i = 0; i<6; i++){
        if(input2[i][col] == c){
            return i;
        }
    }
    return -1;
}

void f(int col){
    if(col == 5){
        dump.insert(build);
        return;
    }

    for(int i = 0; i<6; i++){
        char c = input1[i][col];
        int fc = findChar(c, col);
        if(fc >= 0){
            build.push_back(c);
            f(col+1);
            build.pop_back();
        }
    }
}

int main(){
    int cases;
    scanf("%d", &cases);

    for(int e = 0; e<cases; e++){
        scanf("%d", &K);
        for(int i = 0; i<6; i++){
            scanf("%s", input1[i]);
        }
        for(int i = 0; i<6; i++){
            scanf("%s", input2[i]);
        }

        dump.clear();
        f(0);
        // Print ans
        if(K > dump.size()){
            printf("NO\n");
        } else {
            int i = 1;
            for(auto word : dump){
                if(i == K){
                    cout << word << endl;
                    break;
                }
                i++;
            }

        }

    }

    return 0;
}