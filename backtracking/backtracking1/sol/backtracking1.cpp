#include <bits/stdc++.h>

using namespace std;

void f(int n){
    if(n == 0){
        printf("%d\n", n);
        return;
    }
    printf("%d\n", n);
    f(n-1);
    printf("%d\n", n);
}

int main(){
    f(5);  // 5 4 3 2 1 0 1 2 3 4 5
    return 0;
}
