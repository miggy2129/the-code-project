#include <bits/stdc++.h>

using namespace std;

vector<string> arr({"abc", "de", "fgh"});

int N;

string build;

void f(int index){
    if(index == N){
        cout << build << endl;
        return;
    }

    for(auto letter : arr[index]){
        build.push_back(letter);
        f(index+1);
        build.pop_back();
    }
}

int main(){
    N = arr.size();
    f(0);
}
