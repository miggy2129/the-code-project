## Mat 8. Perform matrix multiplication.

Input format: `N`, `M` and `O`, followed by `N` lines of `M` integers for the first matrix. `M` lines of `O` integers follow for the second matrix.

Input:
```
3 2 4
1 2 
3 4 
5 6 
1 2 9 8 
5 10 3 4 
```
Output:
```
11 22 15 16 
23 46 39 40 
35 70 63 64 
```
Input:
```
3 3 3
1 2 3 
4 5 6 
7 8 9 
1 3 5 
10 3 -3 
1 5 1 
```
Output:
```
24 24 2 
60 57 11 
96 90 20 
```
See [mat8.cpp](mat8.cpp), [mat8.in](mat8.in) and [mat8.output](mat8.output).
