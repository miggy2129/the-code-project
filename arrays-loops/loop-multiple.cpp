// loop-multiple.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    for(int i = 1, j = 2; i<=5; i=i+1, j*=2){
        printf("i = %d, j = %d\n", i,j);
    }

    return 0;
}