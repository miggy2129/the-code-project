// loop-0.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int cnt = 0;
    int i;
    for(i = 0; i<5; i++){
        printf("i=%d. Executing the body.\n", i);
        cnt++;
    }
    printf("The final value of i is %d.\n", i);
    printf("Loop executed %d times.\n", cnt);

    return 0;
}