// pointers.cpp
#include <bits/stdc++.h>

using namespace std;

void double_var(int mynum){
    mynum = mynum*2;  // Doesn't work, as the function has its own 'mynum'
}

void double_var_with_ptr(int * mynum){
    *mynum = (*mynum) * 2;
}

int main(){
    int mynum = 5;
    int * myintptr = &mynum;  // Note the ampersand.

    printf("My num's address is %p\n", myintptr);
    printf("My num is %d\n", mynum);
    *myintptr = 10;
    printf("My num is now %d\n", mynum);

    int *anotherintptr = &mynum;
    int* yetanotherintptr = myintptr;  // You can put the * anywhere

    *anotherintptr = 20;
    printf("My num is modified to %d\n", mynum);

    *yetanotherintptr = 15;
    printf("My num is changed to %d\n", mynum);
    double_var(mynum);
    printf("Tried to modify mynum inside a function: %d\n", mynum);
    double_var_with_ptr(&mynum);  // Note the ampersand
    printf("Doubling it by passing the address of mynum: %d\n", mynum);
    double_var_with_ptr(anotherintptr);  // We pass the pointer
    printf("Doubling it by passing a pointer: %d\n", mynum);
    return 0;
}