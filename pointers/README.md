## Pointers
Pointers are just like your usual data types (int, char, float), except that instead of storing those, it stores the *addresses* of other variables instead.

One use case is in modifying an outer variable from inside a called function, as discussed in the scope section [here](/functions). The most common example is in `scanf`, where we use `&` to pass the address of our variables.

Basic stuff you need to know about pointers:
- You declare a pointer like this: `int * myptr`.
- As mentioned, `&` gives you the address of the variable. Hence if we have an integer called `myint`, we can do `myptr = &myint`.
- We access the original variable by using `*`, as in `*myptr = 5`. This is called dereferencing.
- Arrays decompose into pointers. Hence we use `&` for scanning numbers, but don't need to when scanning C-strings (char arrays).
- As arrays are contiguous blocks of memory, we can increment a pointer to get to a certain element/index. `*(ptr+5)` is the same as `ptr[5]`. This is also the reason why array indices start at 0.
- For [structs](http://www.cplusplus.com/doc/tutorial/structures/), members are accessed with `.`, as in `mystruct.myfield`. If we have a pointer to a struct, then we will do something like `(*mystructptr).myfield`, or alternatively, `mystructptr->myfield`.
- Pointers to pointers exist. Also called double pointers.

See the sample code.

[pointers.cpp](pointers.cpp)
```cpp
// pointers.cpp
#include <bits/stdc++.h>

using namespace std;

void double_var(int mynum){
    mynum = mynum*2;  // Doesn't work, as the function has its own 'mynum'
}

void double_var_with_ptr(int * mynum){
    *mynum = (*mynum) * 2;
}

int main(){
    int mynum = 5;
    int * myintptr = &mynum;  // Note the ampersand.

    printf("My num's address is %p\n", myintptr);
    printf("My num is %d\n", mynum);
    *myintptr = 10;
    printf("My num is now %d\n", mynum);

    int *anotherintptr = &mynum;
    int* yetanotherintptr = myintptr;  // You can put the * anywhere

    *anotherintptr = 20;
    printf("My num is modified to %d\n", mynum);

    *yetanotherintptr = 15;
    printf("My num is changed to %d\n", mynum);
    double_var(mynum);
    printf("Tried to modify mynum inside a function: %d\n", mynum);
    double_var_with_ptr(&mynum);  // Note the ampersand
    printf("Doubling it by passing the address of mynum: %d\n", mynum);
    double_var_with_ptr(anotherintptr);  // We pass the pointer
    printf("Doubling it by passing a pointer: %d\n", mynum);
    return 0;
}
```
Output:
```
My num's address is 0x7ffd4c363364
My num is 5
My num is now 10
My num is modified to 20
My num is changed to 15
Tried to modify mynum inside a function: 15
Doubling it by passing mynum: 30
Doubling it by passing a pointer: 60
```

[pointers2.cpp](pointers2.cpp)
```cpp
// pointers2.cpp
#include <bits/stdc++.h>

using namespace std;

void printStr(char * str){
    int len = strlen(str);
    printf("Printing %d characters: ", len);
    // Can just be printf("%s"), but doing this for illustration
    for(int i = 0; i<len; i++){
        printf("%c", str[i]);  // Notice the equivalence between pointers and arrays
    }
}

void upperStr(char * str){
    for(int i = 0; str[i]; i++){
        str[i] = toupper(str[i]);
    }
}

// As usual arrays don't have a terminating element (i.e. '\0' for C-strings), we can't use functions like strlen and need to pass the size manually.
void printIntArr(int * arr, int n){
    for(int i = 0; i<n; i++){
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(){
    char cstr[100] = "my c-string asdfasdfasdf\n";
    printf("%s", cstr);  // Notice how we don't use &.
    printf("%s", cstr+3);  // As it decomposes into pointers, we can do pointer arithmetic

    char * ptr = cstr;
    printf("%s", ptr);
    printf("%s", ptr+12);  

    printStr(ptr+4);

    // Note the equivalence
    *(ptr+1) = 'Y';
    ptr[3] = 'C';
    printStr(ptr);

    // Modify within a function
    upperStr(ptr);
    printStr(cstr);

    // Also works of ints, or any other array for that matter
    int intArr[] = {1,2,3,10,4,7};
    int numElements = sizeof(intArr)/sizeof(int);
    printIntArr(intArr, numElements);
    printIntArr(intArr+2, numElements-2);
    int * intPtr = intArr;
    printIntArr(intPtr+4, numElements-4);
    char * cptr = (char*)intPtr;  
    cptr += 4;  // chars are 1 byte, ints are 4 bytes
    printIntArr((int*)cptr, numElements-1);    

    return 0;
}
```
Output:
```
my c-string asdfasdfasdf
c-string asdfasdfasdf
my c-string asdfasdfasdf
asdfasdfasdf
Printing 21 characters: -string asdfasdfasdf
Printing 25 characters: mY C-string asdfasdfasdf
Printing 25 characters: MY C-STRING ASDFASDFASDF
1 2 3 10 4 7 
3 10 4 7 
4 7 
2 3 10 4 7 
```
Note the [typecasting](http://www.cplusplus.com/doc/tutorial/typecasting/) between `char *` and `int *` at the end. Chars are 1 byte big, whereas ints take 4 bytes. Hence, when we increment a `char *` by four, that is just equivalent to incremeting an `int *` by one.

[pointers3.cpp](pointers3.cpp)
```cpp
// pointers3.cpp
#include <bits/stdc++.h>

using namespace std;

typedef struct {
    int student_id;
    int phone_number;
} student;

int main(){
    student a;
    a.student_id = 1;
    a.phone_number = 12345;

    printf("%d %d\n", a.student_id, a.phone_number);

    student * ptr = &a;
    printf("%d %d\n", (*ptr).student_id, (*ptr).phone_number);

    printf("%d %d\n", ptr->student_id, ptr->phone_number);    
    return 0;
}
```
Output:
```
1 12345
1 12345
1 12345
```

## Resources
- See the discussion of scope in [functions](/functions).
- [tutorialspoint.com](https://www.tutorialspoint.com/cprogramming/c_pointers.htm).
- Article on [cplusplus.com](http://www.cplusplus.com/articles/z6vU7k9E/).
- Python has its own set of rules. See [here](http://stackoverflow.com/questions/986006/how-do-i-pass-a-variable-by-reference).