## Max/Network Flow
Consider a set of nodes (places), with pipes (or corridors) connecting them. Each pipe has its own capacity, e.g. volume of water per second, or number of people that can pass through per second. What is the maximum flow that can go from one node (the source) to another (the sink)?

## Problems
- [Flooding Fields](https://open.kattis.com/problems/floodingfields)
- [Down Went The Titatnic (UVa 11380)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2375)
- [Monkeys in the Emei Mountain (UVa 11167)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2108)
- [My T-shirt suits me (UVa 11045)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=1986). Standard matching problem.
