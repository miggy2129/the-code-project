## Str 6. Shift a string right. (No using [substr](http://www.cplusplus.com/reference/string/string/substr/).)

Input format: a string on its own line.

Output the shifted string.

Input:
```
some string
```
Output:
```
gsome strin
```
Input:
```
MyString
```
Output:
```
gMyStrin
```
Input:
```
aaabbbbcdefgh
```
Output:
```
haaabbbbcdefg
```
Input:
```
12344567890
```
Output:
```
01234456789
```
Input:
```
random
```
Output:
```
mrando
```
See [str6.cpp](str6.cpp), [str6.in](str6.in) and [str6.output](str6.output).
