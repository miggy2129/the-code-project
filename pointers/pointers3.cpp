// pointers3.cpp
#include <bits/stdc++.h>

using namespace std;

typedef struct {
    int student_id;
    int phone_number;
} student;

int main(){
    // Struct pointers
    student a;
    a.student_id = 1;
    a.phone_number = 12345;

    printf("%d %d\n", a.student_id, a.phone_number);

    student * ptr = &a;
    printf("%d %d\n", (*ptr).student_id, (*ptr).phone_number);

    printf("%d %d\n", ptr->student_id, ptr->phone_number);    
    return 0;
}