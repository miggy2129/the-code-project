void f(index = 0, build=''){
    var arr = ['abc', 'de', 'fgh'];
    var N = arr.length;

    if(index == N){
        alert(build);
        return;
    }

    for(var j = 0; j<arr[index].length; j++) {
        var next_build = build + arr[index][j];
        f(index+1, next_build);
    }

}