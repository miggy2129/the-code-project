#include <bits/stdc++.h>

using namespace std;

#define MAX_N 20

typedef bitset<MAX_N> bset; 

string str = "abc";

int N;

string build;

void f(bset taken){
    if(taken.count() == N){
        cout << build << endl;
        return;
    }

    for(int i = 0; i<N; i++){
        if(!taken[i]){
            bset next_taken = taken;
            next_taken[i] = 1;
            build.push_back(str[i]);
            f(next_taken);
            build.pop_back();
        }
    }
}

int main(){
    N = str.size();
    f(0);

    return 0;
}