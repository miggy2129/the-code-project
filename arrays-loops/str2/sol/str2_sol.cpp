// str2_sol.cpp
#include <bits/stdc++.h>

using namespace std;


int main(){
    string str1, str2;

    while(cin >> str1 >> str2) {
        int ans = -1;

        for(int i = 0; i+str2.size()-1 < str1.size(); i++){
            bool good = true;
            for(int j = 0; j<str2.size(); j++){
                if(str1[i+j] == str2[j]){
                    continue;
                } else {
                    good = false;
                    break;
                }
            }
            if(good){
                ans = i;
                break;
            }
        }

        if(ans == -1){
            printf("NOT FOUND\n");
        } else {
            printf("%d\n", ans);
        }

    }


    return 0;
}