// mat7_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int arr[MAX_N][MAX_N];
    int N;
    int arr2[MAX_N][MAX_N];  // My solution writes it to another array
    char name[] = "mat7";

    while(scanf("%d", &N)!=EOF){
        printf("Input:\n```\n%d\n", N);
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                scanf("%d", &arr[i][j]);
            }
        }
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                printf("%d ", arr[i][j]);
            }
            printf("\n");
        }
        printf("```\nOutput:\n```\n");

        // Your code here
        for(int j = 0; j<N; j++){
            for(int i = 0; i<N; i++){
                arr2[N-1-j][i] = arr[i][j];
            }
        }

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                printf("%d ", arr2[i][j]);
            }
            printf("\n");
        }
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output)\n", name,name,name,name,name,name );

}