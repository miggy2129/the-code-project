## Mat 7. Rotate counterclockwise.

Input format: `N`, followed by `N` lines of `N` integers (square matrix). 

Input:
```
3
1 2 3 
4 5 6 
7 8 9 
```
Output:
```
3 6 9 
2 5 8 
1 4 7 
```
Input:
```
2
1 2 
3 4 
```
Output:
```
2 4 
1 3 
```
Input:
```
4
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
```
Output:
```
4 8 12 16 
3 7 11 15 
2 6 10 14 
1 5 9 13 
```
Input:
```
5
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 
21 22 23 24 25 
```
Output:
```
5 10 15 20 25 
4 9 14 19 24 
3 8 13 18 23 
2 7 12 17 22 
1 6 11 16 21 
```
Input:
```
1
1 
```
Output:
```
1 
```
See [mat7.cpp](mat7.cpp), [mat7.in](mat7.in) and [mat7.output](mat7.output).
