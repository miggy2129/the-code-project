# Greedy solution

The AC greedy code is in [philip-j-fry-greedy.cpp](philip-j-fry-greedy.cpp).

Explanation to follow. [TODO]

DP solution runs in 0.080 s, while this greedy solution runs in only 0.020 s on UVa.