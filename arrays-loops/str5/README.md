## Str 5. Shift a string left. (No using [substr](http://www.cplusplus.com/reference/string/string/substr/).)

Input format: a string on its own line.

Output the shifted string.

Input:
```
some string
```
Output:
```
ome strings
```
Input:
```
MyString
```
Output:
```
yStringM
```
Input:
```
aaabbbbcdefgh
```
Output:
```
aabbbbcdefgha
```
Input:
```
12344567890
```
Output:
```
23445678901
```
Input:
```
random
```
Output:
```
andomr
```
See [str5.cpp](str5.cpp), [str5.in](str5.in) and [str5.output](str5.output).
