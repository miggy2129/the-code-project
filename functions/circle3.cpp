// circle3.cpp
#include <bits/stdc++.h>

using namespace std;

#define PI 3.14

double get_area(double r){
    return PI*r*r;
}

int main(){
    double r = 2.5;
    printf("The area of the circle with radius %f is %f\n", r, get_area(r));

    double another_r = 5;
    printf("The area of another circle with radius %f is %f\n", another_r, get_area(another_r));  // We don't need to type out PI*another_r*another_r, but can just reuse our function.
    return 0;
}