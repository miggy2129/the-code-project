## Strongly Connected Components (SCC's)

## Resources
- [Wikipedia](https://en.wikipedia.org/wiki/Strongly_connected_component)
- [Geeks for Geeks](http://www.geeksforgeeks.org/strongly-connected-components/). Kosaraju's algorithm&mdash;[DFS](../dfs) on original and transpose (edges reversed) graphs.
