## Str 1. Turn a string upper case.

Input format: each string on its own line. 

Output the uppercase equivalent (non-letters should be the same).

*Hint: [toupper](http://www.cplusplus.com/reference/cctype/toupper/?kw=toupper)*

Input:
```
asdfasdjfsdf
```
Output:
```
ASDFASDJFSDF
```
Input:
```
cat
```
Output:
```
CAT
```
Input:
```
DoG
```
Output:
```
DOG
```
Input:
```
mixeeeddCase
```
Output:
```
MIXEEEDDCASE
```
Input:
```
FULLUPPER
```
Output:
```
FULLUPPER
```
Input:
```
SOMEnot
```
Output:
```
SOMENOT
```
Input:
```
HASnumbers23455alksjdfjfj999
```
Output:
```
HASNUMBERS23455ALKSJDFJFJ999
```
Input:
```
something with spaces EVEN 999234;;;;
```
Output:
```
SOMETHING WITH SPACES EVEN 999234;;;;
```
See [str1.cpp](str1.cpp), [str1.in](str1.in) and [str1.output](str1.output).
