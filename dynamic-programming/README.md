**If you don't know [backtracking](/backtracking), learn that first.** Alternatively, improve your backtracking skills by solving DP problems, but at the very least you should be able to do the three backtracking exercises presented in that folder.

For those not familiar, try reading some of the (interesting) problems below, like [Tourist](https://open.kattis.com/problems/tourist) and [Trainsorting](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2451). At first they may seem daunting (especially if you have no idea how to solve them), but you will be surpised later on when you get to solve them, after going through many DP problems.

To get started, read through the [Topcoder tutorial](https://www.topcoder.com/community/data-science/data-science-tutorials/dynamic-programming-from-novice-to-advanced/) below. Also be familiar with the classic problems (just Google "dynamic programming classic problems") like [LIS](http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/), [knapsack](http://www.geeksforgeeks.org/dynamic-programming-set-10-0-1-knapsack-problem/), [coin change](http://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/), [LCS](http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/). Also try out the non-classical problems like [Philip J. Fry](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3746), as well as the others in Ch. 3 and 8 of uHunt. If you can solve such non-classical problems, then you have learned DP.

Also, go through (try to think about) the other problems listed below. While you may may not be able to solve the problem immediately, the solution may come to you as you go learn other DP problems.

## Resources
- [Topcoder tutorial](https://www.topcoder.com/community/data-science/data-science-tutorials/dynamic-programming-from-novice-to-advanced/)
- [Longest Increasing Subsequence (LIS), classic](http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/)
- [Knapsack, classic](http://www.geeksforgeeks.org/dynamic-programming-set-10-0-1-knapsack-problem/)
- [Coin change, classic](http://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/)
- [Longest Common Subsequence (LCS), classic](http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/)

## Problems
- [HackerRank](https://www.hackerrank.com/domains/algorithms/dynamic-programming)
- [Knapsack (Kattis)](https://open.kattis.com/problems/knapsack) - Classic problem. Print solution.
- [Philip J. Fry (UVa 12324)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3746) - Simple problem, good for learning.
- [Flight Planner (UVa 10337)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=1278) - Common problem.
- [Marks Distribution (UVa 10910)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=1851) - Another good starter problem. 
- [Trainsorting (UVa 11456)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2451) - LIS Variant.
- [Tourist (Kattis)](https://open.kattis.com/problems/tourist)
- [The MailBox Manufacturers Problem (UVa 882)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=823) - Fun problem.
- [The Vindictive Coach (UVa 702)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=643) - Uses bitset/bitmask.
- [Concert Tour (UVa 12875)](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=4740) - [ACM-ICPC Bangkok Regionals 2014](https://www.acm-icpc.eng.chula.ac.th/acm-2014/index.php).
- [The Islands (UVa 1096)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&category=24&problem=3537&mosmsg=Submission+received+with+ID+18898053) - Similar to [Tourist](https://open.kattis.com/problems/tourist) in that you have two directed paths from start to destination. World Finals 2010.
- [Free Parentheses (UVa 1238)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3679) - Another fun/quite challenging problem. Discussed in Halim's book.
- [ACORN (UVa 1231)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3672) - Also interesting, requires some optimization. Also discussed in Halim's book.
- [Narrow Art Gallery (Kattis)](https://open.kattis.com/problems/narrowartgallery)
- [Sharing Chocolate (UVa 1099)](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=3540) - One of the harder problems. Requires **lots** of optimization. (Turns out this was actually from the ACM-ICPC World Finals in 2010!)

