// mat2_sol.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000
#define MAX_M 1000

int main(){
    int arr[MAX_N][MAX_M];
    int N, M;

    while(scanf("%d %d", &N, &M)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                scanf("%d", &arr[i][j]);
            }
        }

        // Your code here
        for(int i = 0; i<N/2; i++){
            for(int j = 0; j<M; j++){
                swap(arr[i][j], arr[N-1-i][j]);
            }
        }

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("%d ", arr[i][j]);
            }
            printf("\n");
        }
    }
}