## List
[Lists](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Linked%20Lists/linked%20lists.html) allow for efficient insertion and deletion anywhere, compared to [vectors](../vector) where you will have to move elements in `O(n)`. The catch however is that you'd need to have your [iterators](http://www.cplusplus.com/reference/iterator/) (or if implementing your own, [pointers](/pointers)) in place, whose traversal takes `O(n)`.

Just like with [deques](../deque), we have [front](http://www.cplusplus.com/reference/list/list/front/), [push_front](http://www.cplusplus.com/reference/list/list/push_front/) and [pop_front](http://www.cplusplus.com/reference/list/list/pop_front/), as well as [back](http://www.cplusplus.com/reference/list/list/back/), [push_back](http://www.cplusplus.com/reference/list/list/push_back/) and [pop_back](http://www.cplusplus.com/reference/list/list/pop_back/). More importantly, we can [insert](http://www.cplusplus.com/reference/list/list/insert/) an element before an iterator, as well as [erase](http://www.cplusplus.com/reference/list/list/erase/) an element pointed by an iterator (returns an iterator pointing to the element after the deleted one).

See [list.cpp](list.cpp).
```cpp
// list.cpp
#include <iostream>
#include <list>

using namespace std;

int main(){
    list<int> l;  // []; empty

    l.push_back(3);  // [3]
    l.push_back(5);  // [3,5]
    l.push_back(4);  // [3,5,4]
    l.push_front(6);  // [6,3,5,4]
    l.push_back(7);  // [6,3,5,4,7]
    l.push_back(8);  // [6,3,5,4,7,8]

    for(list<int>::iterator itr = l.begin(); itr!=l.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    list<int>::iterator it = l.begin(); // [<6>,3,5,4,7,8]
    cout << *it << endl;  // 6 

    it++;  // [6,<3>,5,4,7,8]
    cout << *it << endl;  // 3

    advance(it,3);  // [6,3,5,4,<7>,8]; Pretty much it += 3, or it++ repeatedly. Like mentioned, traversal of iterators takes O(n).
    cout << *it << endl;  // 7

    l.insert(it,12);  // [6,3,5,4,12,<7>,8]
    l.insert(it,15);  // [6,3,5,4,12,15,<7>,8]
    for(list<int>::iterator itr = l.begin(); itr!=l.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;
    cout << "it still points to " << *it << endl;  // 7

    advance(it, -4);  // [6,3,<5>,4,12,15,7,8]
    cout << "it now points to " << *it << endl;  // 5

    it = l.erase(it);  // [6,3,<4>,12,15,7,8] returns the element after the deleted one
    cout << "it now points to " << *it << endl;  // 4

    for(list<int>::iterator itr = l.begin(); itr!=l.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    *it = 99;  // [6,3,<99>,12,15,7,8]

    for(list<int>::iterator itr = l.begin(); itr!=l.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    return 0;
}
```
Output:
```
6 3 5 4 7 8 
6
3
7
6 3 5 4 12 15 7 8 
it still points to 7
it now points to 5
it now points to 4
6 3 4 12 15 7 8 
6 3 99 12 15 7 8 
```

Personally, I haven't used lists much; most algorithms and problems usually make use of the other data structures, namely [vectors](../vector), [stacks](../stack), [queues](../queue)/[priority queues](../priority-queue), [sets](../set), and [maps](../map).



## Additional Practice
In Ch. 2 of uHunt, there is only one problem under `list`. To improve coding skill, try implementing linked lists with structs and [pointers](/pointers), if you haven't before.